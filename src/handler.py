import json
import sys
import logging
import os
import psycopg2

# Get Credentials From ENV Variables
rds_host = os.environ['RDS_HOST']
db_user = os.environ['DB_USER']
db_password = os.environ['DB_PASSWORD']
db_name = os.environ['DB_NAME']

# Start Logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

try:
    conn = psycopg2.connect(host=rds_host, user=db_user,
                            password=db_password, database=db_name, connect_timeout=5)
except psycopg2.Error as e:
    logger.error("ERROR: Unexpected error: Could not connect to RDS instance.")
    logger.error(e)
    sys.exit()

logger.info("SUCCESS: Connection to RDS PSQL instance succeeded")


# def createType(conn):
#     # with conn.cursor() as cur:
#     # TODO: Create ENUMS for package_id, travel_type_id
#     # try:
#     #     cur.ececute('CREATE TYPE package_id AS ENUM(1,2,3,4,5,6,7)')
#     # except psycopg2. #maybe exception=DuplicateObject base exception=ProgrammingError
#     return True

# Create Table if Not Exists
def createTable(conn):
    success = False
    try:
        with conn.cursor() as cur:
            cur.execute("""
                CREATE TABLE IF NOT EXISTS
                    Rides(
                            booking_id varchar(255) NOT NULL,
                            user_id varchar(255) NOT NULL,
                            vehicle_model_id varchar(255) NOT NULL,
                            package_id varchar(255) DEFAULT NULL,
                            travel_type_id varchar(255) NOT NULL,
                            from_area_id varchar(255) NOT NULL,
                            to_area_id varchar(255) DEFAULT NULL,
                            from_city_id varchar(255) DEFAULT NULL,
                            to_city_id varchar(255) DEFAULT NULL,
                            from_date DATE NOT NULL,
                            to_date DATE DEFAULT NULL,
                            online_booking BOOLEAN NOT NULL,
                            mobile_site_booking BOOLEAN NOT NULL,
                            booking_created DATE NOT NULL,
                            from_lat varchar(255) NOT NULL,
                            from_long varchar(255) NOT NULL,
                            to_lat varchar(255) DEFAULT NULL,
                            to_long varchar(255) DEFAULT NULL,
                            Car_Cancellation BOOLEAN NOT NULL DEFAULT '0',
                            PRIMARY KEY (booking_id))""")
        conn.commit()
        logger.info("SUCCESS: Table Creation/Check Successful")
        success = True
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not create Table")
        logger.error(e)
        return success

    return success

# Form Query for Postgresql
def formatQuery(data):
    variables = ''
    values = ''
    for key in data:
        if(data[key] != 'NULL'):
            variables += key + ' '
            values += '\'' + data[key] + '\' '

    variables = variables.strip().replace(' ', ', ')
    values = values.strip().replace(' ', ', ')

    query = 'INSERT INTO Rides({}) VALUES({})'.format(variables, values)

    return query

# Insert Data to Database
def insertData(conn, data):
    success = False
    error = None
    try:
        query = formatQuery(data)

        with conn.cursor() as cur:
            cur.execute(query)
        conn.commit()
        logger.info(
            "SUCCESS: Data Saved Successfully!, %s", (data['booking_id']))
        success = True

    except psycopg2.IntegrityError as e:
        conn.rollback()
        logger.error(
                    "ERROR: DUPLICATE error: Could not insert into Table! %s", (data['booking_id']))
        logger.error(e)
        error = "Duplicate Data"

    except Exception as e:
        conn.rollback()
        logger.error(
            "ERROR: Unexpected error: Could not insert into Table! %s", (data['booking_id']))
        logger.error(e)
        error = 'NOT DISCLOSED'

    return success, error

# Check if value is empty
def isEmpty(value):
    if (value == None):
        return False
    return len(str(value)) > 0

# Check if value is integer or string
def isStrOrNum(value):
    if(value == None):
        return False
    return isinstance(value, (str, int))

# Remove NULLs from provided data
def validateData(data):
    invalids = ""
    for key in data:
        if(not (isEmpty(data[key]) and isStrOrNum(data[key]))):
            invalids += key + ','
    return invalids


def processAndSaveData(data):
    return_msg = None
    return_flag = False
    status_code = 400

    invalids = validateData(data)
    isValid = len(invalids.strip()) == 0

    if(isValid):
        # isType = createType(conn)
        isTable = createTable(conn)
        if(isTable):
            isInserted, error = insertData(conn, data)
            if(isInserted):
                return_flag = True
                status_code = 201
            return_msg = {
                "success": isInserted,
                "error": error,
                "data":  data
            }
        else:
            status_code = 500
            return_msg = {
                "success": False,
                "msg": "Internal Server Error"
            }
    else:
        logger.error(
            "ERROR: Invalid Data Provided with booking_id: %s", (data["booking_id"]))
        return_msg = invalids + ' not valid'

    return return_msg, return_flag, status_code


def extractData(body):
    return {
        "booking_id": body.get("booking_id"),
        "user_id": body.get("user_id"),
        "vehicle_model_id": body.get("vehicle_model_id"),
        "package_id": body.get("package_id"),
        "travel_type_id": body.get("travel_type_id"),
        "from_area_id": body.get("from_area_id"),
        "to_area_id": body.get("to_area_id"),
        "from_city_id": body.get("from_city_id"),
        "to_city_id": body.get("to_city_id"),
        "from_date": body.get("from_date"),
        "to_date": body.get("to_date"),
        "online_booking": body.get("online_booking"),
        "mobile_site_booking": body.get("mobile_site_booking"),
        "booking_created": body.get("booking_created"),
        "from_lat": body.get("from_lat"),
        "from_long": body.get("from_long"),
        "to_lat": body.get("to_lat"),
        "to_long": body.get("to_long"),
        "Car_Cancellation": body.get("Car_Cancellation")
    }


def saveXRide(event, context):
    return_msg = None
    return_flag = False
    return_service = 'XRidesData'
    status_code = 400

    body = event.get("body")
    body = json.loads(body)

    data = extractData(body)

    return_msg, return_flag, status_code = processAndSaveData(data)

    body = {
        "message": return_msg,
        "flag": return_flag,
        "service_used": return_service
    }

    response = {
        "statusCode": status_code,
        "body": json.dumps(body)
    }

    return response
